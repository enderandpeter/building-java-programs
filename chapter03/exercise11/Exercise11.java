package chapter03.exercise11;

import java.util.Scanner;

public class Exercise11 {
    public static void main(String args[]){
        System.out.println("Find the distance between two points: (x1, y1), (x2, y2).");

        double x1, y1, x2, y2;
        if(args.length == 4){
            x1 = Double.parseDouble(args[0]);
            y1 = Double.parseDouble(args[1]);
            x2 = Double.parseDouble(args[2]);
            y2 = Double.parseDouble(args[3]);
            
        } else {
            Scanner console = new Scanner(System.in);

            System.out.print("Enter x1: ");
            x1 = console.nextDouble();

            System.out.print("Enter y1: ");
            y1 = console.nextDouble();

            System.out.print("Enter x2: ");
            x2 = console.nextDouble();

            System.out.print("Enter y2: ");
            y2 = console.nextDouble();
            console.close();

        }
        
        distance(x1, y1, x2, y2);
    }

    public static void distance(double x1, double y1, double x2, double y2){
        System.out.println("The distance is: " + Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)));
    }
}