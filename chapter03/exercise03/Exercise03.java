package chapter03.exercise03;

public class Exercise03 {
    public static void main(String[] args){
        int base = Integer.parseInt(args[0]);
        int maxPower = Integer.parseInt(args[1]);
        if(maxPower < 0){
            System.err.println("Please enter a number that is 0 or greater");
        }

        printPowersOfN(base, maxPower);
    }

    public static void printPowersOfN(int base, int power){        
        for(int i = 0; i <= power; i++){
            System.out.print((int) Math.pow(base, i) + " ");            
        }
        System.out.println();
    }
}