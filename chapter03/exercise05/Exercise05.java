package chapter03.exercise05;

public class Exercise05 {
    public static void main(String args[]){
        int first = Integer.parseInt(args[0]);
        int last = Integer.parseInt(args[1]);
        printGrid(first, last);
    }

    public static void printGrid(int first, int last){
        for(int i = 1; i <= first; i++){
            for(int j = 1; j <= last; j++){
                System.out.print(i + first * ( j - 1 ) + " ");
            }
            System.out.println();
        }
    }
}