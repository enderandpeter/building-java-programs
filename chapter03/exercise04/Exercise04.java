package chapter03.exercise04;

/*
 * Print numbers from a minimum to a maximum in a square (or rectangle) shape
 * starting with the original sequence, followed by the first number in the
 * previous line unshifted and pushed onto the end of sequence, until you get
 * to a sequence with the last number prepeneded.
 * 
 * For example:
 * 34567
 * 45673
 * 56734
 * 67345
 * 73456
 * 
 * Example Usage:
 * 
 * java Exercise04 3 7
 */
public class Exercise04{
    public static void main(String[] args){
        int first = Integer.parseInt(args[0]);
        int last = Integer.parseInt(args[1]);
        printSquare(first, last);
    }

    public static void printSquare(int first, int last){
        int squareLength = last - first + 1;
        for(int lineIndex = 0; lineIndex < squareLength; lineIndex++){
            for(int i = first + lineIndex; i <= last; i++){
                System.out.print(i);
            }
            for(int j = 0; j < lineIndex; j++){
                System.out.print(first + j);
            }
            System.out.println();
        }
    }
}