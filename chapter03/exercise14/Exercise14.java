package chapter03.exercise14;

import java.util.Scanner;

public class Exercise14 {
    public static void main(String args[]){
        System.out.println("Get the surface area of a cylinder, provided a radius and height.");

        double radius, height;
        if(args.length == 2){
            radius = Double.parseDouble(args[0]);
            height = Double.parseDouble(args[1]);
        } else {
            Scanner console = new Scanner(System.in);

            System.out.print("Enter radius: ");
            radius = console.nextDouble();
            
            System.out.print("Enter the height: ");
            height = console.nextDouble();

            console.close();

        }
        
        cylinderSurfaceArea(radius, height);
    }

    public static void cylinderSurfaceArea(double radius, double height){        
        System.out.println("Surface area: " + (2 * Math.PI * Math.pow(radius, 2) + 2 * Math.PI * radius * height));
    }
}