package chapter03.exercise10;

import java.util.Scanner;

public class Exercise10 {
    public static void main(String args[]){
        System.out.println("Find the area of a circle with the given radius.");

        double number;
        if(args.length == 1){
            number = Double.parseDouble(args[0]);
            
        } else {
            Scanner console = new Scanner(System.in);

            System.out.print("Enter the radius: ");
            number = console.nextDouble();

            console.close();

        }
        
        area(number);
    }

    public static void area(double radius){
        System.out.println("The area is: " + Math.PI * radius * radius);
    }
}