package chapter03.exercise12;

import java.util.Scanner;

public class Exercise12 {
    public static void main(String args[]){
        System.out.println("Show the computed number from a given base and exponent, as in scientific notation.");

        double base;
        int power;
        if(args.length == 2){
            base = Double.parseDouble(args[0]);
            power = Integer.parseInt(args[1]);
        } else {
            Scanner console = new Scanner(System.in);

            System.out.print("Enter the base: ");
            base = console.nextDouble();
            
            System.out.print("Enter an integer for the power of 10: ");
            power = console.nextInt();

            console.close();

        }
        
        scientific(base, power);
    }

    public static void scientific(double base, int power){
        System.out.println("The value is: " + base * Math.pow(10, power));
    }
}