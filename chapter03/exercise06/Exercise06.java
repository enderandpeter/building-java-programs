package chapter03.exercise06;

public class Exercise06 {
    public static void main(String args[]){
        int first = Integer.parseInt(args[0]);
        int last = Integer.parseInt(args[1]);
        largerAbsVal(first, last);
    }

    public static void largerAbsVal(int first, int last){
        System.out.println(Math.max(Math.abs(first), Math.abs(last)));
    }
}