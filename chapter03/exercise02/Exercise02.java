package chapter03.exercise02;

public class Exercise02 {
    public static void main(String[] args){
        int max = Integer.parseInt(args[0]);
        if(max < 0){
            System.err.println("Please enter a number that is 0 or greater");
        }
        printPowersOf2(max);
    }

    public static void printPowersOf2(int power){        
        for(int i = 0; i <= power; i++){
            System.out.print((int) Math.pow(2, i) + " ");            
        }
        System.out.println();
    }
}