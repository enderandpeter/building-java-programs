package chapter03.exercise07;

public class Exercise07 {
    public static void main(String args[]){
        int first = Integer.parseInt(args[0]);
        int second = Integer.parseInt(args[1]);
        int third = Integer.parseInt(args[2]);
        largerAbsVal(first, second, third);
    }

    public static void largerAbsVal(int first, int second, int third){
        int firstMax = Math.max(Math.abs(first), Math.abs(second));
        System.out.println(Math.max(Math.abs(firstMax), Math.abs(third)));
    }
}