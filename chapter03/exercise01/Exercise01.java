package chapter03.exercise01;

/*
 * Print numbers from 1 to the provided maximum inclosed in 
 * square brackets.
 * 
 * Example Usage:
 * 
 * java Exercise01 10
 * 
 * [1] [2] [3] [4] [5] [6] [7] [8] [9] [10]
 */
public class Exercise01 {
    public static void main(String[] args){
        printNumbers(Integer.parseInt(args[0]));
    }

    public static void printNumbers(int max){
        if(max <= 0){
            System.err.println("max must be an integer greater than zero"); 
            return;
        }

        for(int i = 1; i <= max; i++){
            System.out.print("[" + i + "] ");            
        }
        System.out.println();
    }
}