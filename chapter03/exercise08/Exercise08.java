package chapter03.exercise08;

import java.util.Scanner;

public class Exercise08 {
    public static void main(String args[]){
        System.out.println("Find zeros for a quadratic equation: ax^2 + bx + c");

        int a, b, c;
        if(args.length == 3){
            a = Integer.parseInt(args[0]);
            b = Integer.parseInt(args[1]);
            c = Integer.parseInt(args[2]);
        } else {
            Scanner console = new Scanner(System.in);

            System.out.print("Enter a: ");
            a = console.nextInt();

            System.out.print("Enter b: ");
            b = console.nextInt();

            System.out.print("Enter c: ");
            c = console.nextInt();

            console.close();

        }
        
        quadratic(a, b, c);
    }

    public static void quadratic(int a, int b, int c){
        double plus = (-b + Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / 2 * a;
        double minus = (-b - Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / 2 * a;

        System.out.println("x = " + plus + " and x = " + minus);
    }
}