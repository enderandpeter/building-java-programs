package chapter03.exercise13;

import java.util.Scanner;

public class Exercise13 {
    public static void main(String args[]){
        System.out.println("Compute total pay based on given hourly wage and hours. Hours above 8 recieve overtime pay of 1.5 times the given hourly wage.");

        double wage;
        int hours;
        if(args.length == 2){
            wage = Double.parseDouble(args[0]);
            hours = Integer.parseInt(args[1]);
        } else {
            Scanner console = new Scanner(System.in);

            System.out.print("Enter the hourly wage: ");
            wage = console.nextDouble();
            
            System.out.print("Enter the hours worked as an integer: ");
            hours = console.nextInt();

            console.close();

        }
        
        pay(wage, hours);
    }

    public static void pay(double wage, int hours){
        final int OVERTIME = 8;
        double pay = wage * hours;
        if(hours > OVERTIME){
            pay += (0.5 * wage) * (hours - OVERTIME);
        }
        System.out.println("Total pay: " + pay);
    }
}