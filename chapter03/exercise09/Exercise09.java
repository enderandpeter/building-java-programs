package chapter03.exercise09;

import java.util.Scanner;

public class Exercise09 {
    public static void main(String args[]){
        System.out.println("Find last digit of an integer");

        int number;
        if(args.length == 1){
            number = Integer.parseInt(args[0]);
            
        } else {
            Scanner console = new Scanner(System.in);

            System.out.print("Enter the integer: ");
            number = console.nextInt();

            console.close();

        }
        
        lastDigit(number);
    }

    public static void lastDigit(int number){
        System.out.println("The last digit is: " + number % 10);
    }
}